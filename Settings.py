import os

RESOURCES_PATH = os.path.join(os.path.dirname(__file__), "resources")

STOP_LIST_PATH = os.path.join(RESOURCES_PATH, 'stop.txt')
WORDS_FORMS_PATH = os.path.join(RESOURCES_PATH, 'odm.txt')
DICTIONARY_PATH = os.path.join(RESOURCES_PATH, 'dictionary4')
TFIDF_PATH = os.path.join(RESOURCES_PATH, 'tfidf4')
LDA_PATH = os.path.join(RESOURCES_PATH, 'lda4')
